import angr
import batl.utils
import batl.tbl_detector
import batl.hoaes_helpers
from batl.uf_app import SimConcretizationStrategyUfApp
from functools import reduce
import os
import sys
import pdb
import argparse
import pickle
import IPython
from collections import defaultdict
import time
import json

def pregs(state):
     print("r0", state.regs.r0)
     print("r1", state.regs.r1)
     print("r2", state.regs.r2)
     print("r3", state.regs.r3)
     print("r4", state.regs.r4)
     print("r5", state.regs.r5)
     print("r6", state.regs.r6)
     print("r7", state.regs.r7)
     print("r8", state.regs.r8)
     print("r9", state.regs.r9)
     print("r10", state.regs.r10)
     print("r11", state.regs.r11)
     print("r12", state.regs.r12)
     print("lr", state.regs.lr)
     print("ip", state.regs.ip)

     
parser = argparse.ArgumentParser(description='BATL driver for secinv.')
parser.add_argument("-elf_file", help="Binary of secinv to analyze.", type=str, required=True)
parser.add_argument("-shares", help="Number of masking shares.", type=int, required=True)
parser.add_argument("-inputs", help="Number of keys/plaintexts given to secinv as input.", type=int, required=True)
parser.add_argument("-out", help="The output file to store PLPs.", type=str, required=True)
parser.add_argument("-init_only", help="Stop after init, don't do analysis", action='store_true')
parser.add_argument("-cc", help="The toolchain that compiled the binary.", choices=['llvm', 'gnu'], type=str)
parser.add_argument("-opt", help="The optimization level used in compilation.", type=str, choices=['0', '1', '2'])
args = parser.parse_args()

dirname = os.path.dirname(os.path.abspath('__file__'))

elf_file = args.elf_file
dirname = os.path.dirname(os.path.abspath(elf_file))
proj = angr.Project(elf_file)

# Stubs for Random Number Generation routines.
proj.hook_symbol('myInitRand', batl.hoaes_helpers.myInitRand())
proj.hook_symbol('InitRand', batl.hoaes_helpers.myInitRand())
proj.hook_symbol('KeccakF', batl.hoaes_helpers.KeccakF())

# Stub for Input reading routines. Responsible for labeling the secret.
proj.hook_symbol('GET32', batl.hoaes_helpers.GET32())


to_remove = {angr.options.SIMPLIFY_REGISTER_WRITES,
             angr.options.SIMPLIFY_REGISTER_READS,
             angr.options.CONSERVATIVE_READ_STRATEGY,
             angr.options.OPTIMIZE_IR}

to_add = {angr.options.TRACK_OP_ACTIONS,
          angr.options.TRACK_REGISTER_ACTIONS}.union(angr.options.refs)



# Some useful symbol names
stop_timer_addr = proj.loader.find_symbol("stop_timer").rebased_addr
combinedsbox_addr = proj.loader.find_symbol("CombinedSbox").rebased_addr
rand_array_addr = proj.loader.find_symbol("rand_array").rebased_addr
rand_index_addr = proj.loader.find_symbol("rand_index").rebased_addr
retrand_addr = proj.loader.find_symbol("RetRand").rebased_addr
dualsbox_addr = proj.loader.find_symbol("DualSbox").rebased_addr
notmain_addr = proj.loader.find_symbol("notmain").rebased_addr
mask_array_addr = proj.loader.find_symbol("MaskArray").rebased_addr
start_addr = proj.loader.find_symbol("_start").rebased_addr
encrypt_addr = proj.loader.find_symbol("Encrypt").rebased_addr
entry_state = proj.factory.call_state(start_addr,
                                      add_options=to_add,
                                      remove_options=to_remove)
usart_base = 0x40013800
usart_dr = usart_base + 0x04


entry_state.globals['rnd_cnt'] = 0
entry_state.globals['get_cnt'] = 0
entry_state.globals['init_rnd']= 50
entry_state.globals['init_key'] = args.inputs
entry_state.globals['init_txt'] = args.inputs
entry_state.globals['num_shares'] = args.shares
entry_state.globals['key_input_mask_xors'] =  args.inputs*2*(entry_state.globals['num_shares']-1)
entry_state.globals['share_creation_xors'] =  args.inputs*(entry_state.globals['num_shares'])
entry_state.globals['key_txt_xors'] =  entry_state.globals['num_shares'] 
entry_state.globals['regs_blacklist'] = [72, 76, 80, 84]
entry_state.globals['xor_cnt'] = 0
entry_state.globals['shares_bvs'] = [[] for i in range(args.inputs)]
entry_state.globals['elf_file'] = elf_file
entry_state.globals['llvm_opt'] = True if args.cc=='llvm' and args.opt=='2' else False
entry_state.globals['addr_cnt'] = defaultdict(int)
entry_state.globals['mask_array_addr'] = mask_array_addr
entry_state.globals['rand_array_addr'] = rand_array_addr
entry_state.globals['rand_index_addr'] = rand_index_addr
entry_state.globals['share_order_llvm_opt'] = []
entry_state.globals['cnt'] = 0
entry_state.globals['cnt2addr'] = defaultdict(int)
entry_state.globals['addr2dead'] = defaultdict(int)

print(entry_state.globals['init_key'])
simgr = proj.factory.simgr(entry_state)

## Enabling share creation breakpoint.
shares_bp = entry_state.inspect.b('reg_write', when=angr.BP_AFTER,
                               condition=batl.hoaes_helpers.is_xor,
                               action=batl.hoaes_helpers.cnt_xors)

symbol_addresses = {}
for symbol in proj.loader.main_object.symbols:
     if symbol.section is None:
          continue

     if (proj.loader.main_object.sections.raw_list[symbol.section].name != ".rodata") and (proj.loader.main_object.sections.raw_list[symbol.section].name != ".bss"):
          continue
     s_name = symbol.demangled_name
     s_addr_bv = simgr.active[0].solver.BVV(symbol.rebased_addr, 32)
     symbol_addresses[symbol.rebased_addr] = s_name, s_addr_bv
     

uf_app_strat = SimConcretizationStrategyUfApp(1024, symbol_addresses, simgr.active[0].solver.BVV(0, 32))
simgr.active[0].memory.read_strategies.insert(0, uf_app_strat)

start_time = time.time()
while simgr.active[0].addr != combinedsbox_addr:
     print(simgr.active)
     simgr.step(num_inst=1)



## Enabling TBL detector.
print("Disabling shares-reading plugin, enabling TBL plugin.")
simgr.active[0].inspect.remove_breakpoint('reg_write',bp=shares_bp)
tbl_plugin = batl.tbl_detector.TBLPlugin(entry_state.solver.BVV(0, 32),
                                         simgr.active[0].globals['shares_bvs'],
                                         proj.arch.register_names,
                                         vbl=True)

simgr.active[0].register_plugin('tbl_plugin', tbl_plugin)


simgr.active[0].inspect.b('reg_write', when=angr.BP_BEFORE,
                      action=batl.tbl_detector.update_regs)
simgr.active[0].inspect.b('reg_write', when=angr.BP_AFTER,
                      action=batl.tbl_detector.update_tbl)

if not args.init_only:
     ## Starting Leakage Detection.
     stop_addr = 0x80003d3 if simgr.active[0].globals['llvm_opt'] else stop_timer_addr
     while True:
          state = simgr.active[0]
          print("Exploring state with addr", hex(simgr.active[0].addr))
          if state.addr == stop_addr:
               print("Found stop addr")
               break
          state.globals['addr_cnt'][simgr.active[0].addr] += 1
          state.globals['cnt'] += 1
          state.globals['cnt2addr'][state.globals['cnt']] = state.addr
          if state.addr in state.globals['addr2dead']:
               state.globals['addr2dead'][state.addr] = state.globals['addr2dead'][state.addr] + [ set() ]
          else:
               state.globals['addr2dead'][state.addr] = [ set() ]
          simgr.step(num_inst=1)


     symex_elapsed_time = time.time() - start_time

     for lp in state.tbl_plugin.leaky_points:
          lp.total_visit_cnt = state.globals['addr_cnt'][lp.new_write_addr]
          dead_reg_lst = state.globals['addr2dead'][lp.new_write_addr]
          dead_regs = set.intersection(*dead_reg_lst)
          if dead_regs:
               print("PTBL{0}".format(lp.lp_id))
               print("dead_regs:{0}".format(dead_regs))
               lp.patch_reg = dead_regs.pop()
               print("reg {0} can patch PLPP{1}".format(lp.patch_reg,lp.lp_id))
          else:
               print("no dead reg found for PLPP{0}".format(lp.lp_id))
     
     print("Storing plps in", args.out)
     plps=(simgr.active[0].tbl_plugin.leaky_points, simgr.active[0].tbl_plugin.vbl_leaky_points)
     with open(args.out, 'wb') as f:
          pickle.dump(plps, f)

     batl.utils.print_plps(args.out)

     data={}
     data['symex_t'] = symex_elapsed_time
     profile_data_f = os.path.join(dirname, "profile_data.txt")
     with open(profile_data_f, 'w') as jsonfile:
          json.dump(data, jsonfile)

