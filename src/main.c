#include <stdint.h>
#include "stm32f103.h"
#include "masked_combined.h"
#include "KeccakRNG.h"
#include "pl011.h"
#include "ila.h"
extern unsigned char rand_array[200];
extern unsigned char rand_index;
extern unsigned char RetRand();
extern void InitRand(unsigned char *rand_state);


#ifndef NUM_RANDS
#define NUM_RANDS 0
#endif

#define WORD_SIZE 32
#define WORD_RANGE 256

#define UART0BASE 0x4000C000
uint8_t vhw[WORD_RANGE][WORD_SIZE+1];
/* float e[WORD_RANGE]; */
/* float f,v; */

void reset_timer(){
	unsigned int ra;
	
	ra = GET32(SCB_DEMCR);
	ra = ra | 0x01000000;
	PUT32(SCB_DEMCR, ra);
	PUT32(DWT_CYCCNT, 0);
	PUT32(DWT_CONTROL, 0);
}


void start_timer(){
	unsigned int ra;
	ra = GET32(DWT_CONTROL);
	ra = ra | 0x00000001;
	PUT32(DWT_CONTROL, ra);
	//    *DWT_CONTROL = *DWT_CONTROL | 1 ; // enable the counter
}

void stop_timer(){
	unsigned int ra;
	ra = GET32(DWT_CONTROL);
	ra = ra & 0xFFFFFFFE;
	PUT32(DWT_CONTROL, ra);
	//*DWT_CONTROL = *DWT_CONTROL | 0 ; // disable the counter    
}

unsigned int getCycles(){
	return GET32(DWT_CYCCNT);
}

void TriggerOn()
{
	unsigned int ra;
	ra = GET32(GPIOA_ODR);
	ra = ra & 0xFFFFFFFE;		//PA0 = 0
	PUT32(GPIOA_ODR,ra);
}

void TriggerOff()
{
	unsigned int ra;
	ra = GET32(GPIOA_ODR);
	ra = ra | 0x00000001;		//PA0 = 0
	PUT32(GPIOA_ODR,ra);
}

// PA0 is the pin for trigger
void SetupTrigger()
{
	unsigned int ra;
	ra = GET32(GPIOA_CRL);
	ra = ra & 0xFFFFFFF0;		//clear GPIOA_CRL, PA0 
	ra = ra | 0x00000003;		//output mode, 50MHZ, general push up
	PUT32(GPIOA_CRL,ra);
    
	TriggerOff();
}

int notmain(int argc, char *argv[])
{


	int i, j;
	unsigned int ra, num_cycles;

	ra=GET32(RCC_APB2ENR);
	ra|=1<<2;   //GPIOA
	ra|=1<<4;   //GPIOC
	ra|=1<<14;  //USART1
	PUT32(RCC_APB2ENR,ra);
	
	
	// setup UART1
	// pa9 TX  alternate function output push-pull
	// pa10 RX configure as input floating
	ra=GET32(GPIOA_CRH);
	ra&=~(0xFF0);
	ra|=0x490;
	PUT32(GPIOA_CRH,ra);
	
	PUT32(USART1_CR1,0x2000);
	PUT32(USART1_CR2,0x0000);
	PUT32(USART1_CR3,0x0000);
	//8000000/16 = 500000
	//500000/115200 = 4.34
	//4 and 5/16 = 4.3125
	//4.3125 * 16 * 115200 = 7948800
	PUT32(USART1_BRR,0x0045);
	PUT32(USART1_CR1,0x200C);
	
	PUT32(STK_CTRL,4);
	PUT32(STK_LOAD,0x00FFFFFF);
	PUT32(STK_CTRL,5);

	SetupTrigger();

	// Get SHA-3 PRNG seed through UART
	unsigned int KeccakState[5 * 5 * 2];
	unsigned int KeccakIn[5 * 5 * 2];
	unsigned char rand_state[200];
	for(i=0; i<50; i++)
	{
		while(1)
		{
			if(GET32(USART1_SR)&0x20)
				break;
		}
		ra=GET32(USART1_DR)&0xFF;
		
		KeccakState[i] = ra;
	}
	

	
	/* uint8_t p[N][D], k[N][D], c[N][D], x[D], one[D]; */
	/* uint8_t resinv, pk, resone; */
	
	

	/* for (i = 0; i < N; i++) */
	/* { */
	/* 	for (j = 0; j < D; j++) */
	/* 	{ */
	/* 		p[i][j] = 0; */
	/* 		k[i][j] = 0; */
	/* 		c[i][j] = 0; */
	/* 	} */
	/* } */
	/* for (j = 0; j < D; j++) */
	/* { */
	/* 	x[j] = 0; */
	/* 	one[j] = 0; */
	/* } */
	
	
	
	
	while(1)
	{		
		//Re-generate random numbers using Keccak, and store them into random array
		for(i=0; i<50; i++)
		{
			KeccakIn[i] = KeccakState[i];			
		}
		KeccakF(KeccakState, KeccakIn, 25);

		rand_index = 0; 
		j = 0;
		for(i=0; i<50; i++)
		{
			rand_state[j] = KeccakState[i] & 0xFF;			j++;
			rand_state[j] = ( KeccakState[i] >>  8) & 0xFF;	j++;
			rand_state[j] = ( KeccakState[i] >> 16) & 0xFF;	j++;
			rand_state[j] = ( KeccakState[i] >> 24) & 0xFF;	j++;
		}
		InitRand(rand_state);



		/* uint8_t p[N][D], k[N][D], c[N][D], x[D], one[D]; */
		/* uint8_t resinv, pk, resone; */
		uint8_t key[16], input[16], output[16];


		for (i=0; i < 16; i++)
		{
			while(1)
			{
				if(GET32(USART1_SR)&0x20)
					break;
			}
			key[i] = GET32(USART1_DR)&0xFF;
	
		}

		for (i=0; i < 16; i++)
		{
			while(1)
			{
				if(GET32(USART1_SR)&0x20)
					break;
			}
			input[i] = GET32(USART1_DR)&0xFF;
	
		}


		TriggerOn();
		reset_timer(); //reset timer		
		start_timer();
		Encrypt(output, input, key);
		stop_timer();
		num_cycles = getCycles();
		TriggerOff();

		/* // masked key addition and inversion  */
		/* //for (i = 0; i < N; i++) */
		/* i=0;		 */
		/* { */
		/* 	// removing pk for better BATL. consider if needed when evaluating runtime. */
		/* 	for (j = 0; j < D; j++) */
		/* 		x[j] = p[i][j] ^ k[i][j]; */
			
		/* 	TriggerOn(); */
		/* 	reset_timer(); //reset timer		 */
		/* 	start_timer(); */
		/* 	//timul(x, x, c[i]); */
		/* 	secinv(x, c[i], secmul); */
		/* 	stop_timer(); */
		/* 	num_cycles = getCycles(); */
		/* 	TriggerOff(); */
			
		/* 	secmul(x, c[i], one); */

		/* 	resinv = 0; */
		/* 	resone = 0; */
		/* 	pk = 0; */
		/* 	for (j = 0; j < D; j++) */
		/* 	{ */
		/* 		pk ^= x[j]; */
		/* 		resinv ^= c[i][j]; */
		/* 		resone ^= one[j]; */
		/* 	} */
		/* } */

		
		// Upload result through uart
		{
			/* while(1) */
			/* 	if(GET32(USART1_SR)&0x80)  */
			/* 		break; */
			/* PUT32(USART1_DR, output[0]); */
			
			/* while(1) */
			/* 	if(GET32(USART1_SR)&0x80)  */
			/* 		break; */
			/* PUT32(USART1_DR, output[1]); */
			
			/* while(1) */
			/* 	if(GET32(USART1_SR)&0x80)  */
			/* 		break; */
			/* PUT32(USART1_DR, output[2]); */
			
			while(1)
				if(GET32(USART1_SR)&0x80) 
					break;
			PUT32(USART1_DR, num_cycles&0xff);
			
			while(1)
				if(GET32(USART1_SR)&0x80) 
					break;
			PUT32(USART1_DR, (num_cycles>>8)&0xff);
			
		
			while(1)
				if(GET32(USART1_SR)&0x80) 
					break;
			PUT32(USART1_DR, (num_cycles>>16)&0xff);
			
			while(1)
				if(GET32(USART1_SR)&0x80) 
					break;
			PUT32(USART1_DR, (num_cycles>>24)&0xff);
			
		}
	
		

	}
	return 0;

}






int qemumain()
{
    ila();
    
	// RESET code to exit from qemu
#define SYSRESETREQ    (1<<2)
#define VECTKEY        (0x05fa0000UL)
#define VECTKEY_MASK   (0x0000ffffUL)
#define AIRCR          (*(uint32_t*)0xe000ed0cUL) // fixed arch-defined address
#define REQUEST_EXTERNAL_RESET (AIRCR=(AIRCR&VECTKEY_MASK)|VECTKEY|SYSRESETREQ)

    REQUEST_EXTERNAL_RESET;

    return(0);

}
  

