# High Order AES

An AES implementation that uses the Common Shares [1] and  Random Reduction methods [2], obtained from `https://github.com/knarfrank/Higher-Order-Masked-AES-128.git`.


1. Further Improving Efficiency of Higher-Order Masking Schemes by Decreasing Randomness Complexity
Shuang Qiu, Rui Zhang, Yongbin Zhou, Wei Cheng

2. Faster Evaluation of SBoxes via Common Shares
Jean-Sebastien Coron, Aurelien Greuet, Emmanuel Prouff, Rina ZeitounOB

